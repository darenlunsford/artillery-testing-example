# Artillery Testing Example

General setup for artillery testing using external json file for the post payload

## Getting started

Requirements

- artillery io
- accessible api

## Key features

Basic authentication is being used. To update user and password change the values in the load-test.yml file.

## Quick commands

Generic start

```
artillery run load-test.yml
```

### Start with debug options

Print out all http info

```
DEBUG=http artillery run load-test.yml
```

Print out all http request info

```
DEBUG=http:request artillery run load-test.yml
```

Print out all http response info

```
DEBUG=http:response artillery run load-test.yml
```

Print out all http request info

```
DEBUG=http\* artillery run load-test.yml
```

## Reporting

**Artillery** provides built in reporting by adding -o some_report.json where 'some_report' is the name of the file that will be generated in the same location as the load-test.yml file.

```
artillery run -o some_report.json load-test.yml
```

The produced file can be converted to a visual report by using the open source tool online or running it locally.

![Report Example](report-example.png)

[Artillery Report Viewer](https://reportviewer.artillery.io)

[Artillery Report Repo](https://github.com/artilleryio/report-viewer)

## Resources

- [Artillery](https://www.artillery.io/)
- [Artillery Report Viewer](https://reportviewer.artillery.io/)
