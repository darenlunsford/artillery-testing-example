const fs = require('fs');
const filePath = './post-data.txt';
let postData;

/**
 * https://github.com/artilleryio/artillery/issues/791
 */

function setJSONBody(requestParams, context, ee, next) {
  // randomize record to use from data file
  requestParams.body = postData[Math.floor(Math.random() * postData.length)];
  return next();
}

function logHeaders(requestParams, response, context, ee, next) {
  //console.log('headers', requesetParams.headers);
  return next();
}

const generatePostBody = async (context, events, done) => {
  try {
    if (postData === undefined || postData === null || postData.length === 0) {
      postData = await loadDataFromTxt(filePath);
    }

    const postBodyStr = postData[Math.floor(Math.random() * postData.length)];
    context.vars.data = JSON.parse(postBodyStr);
    return done();
  } catch (err) {
    console.log(`Error occurred in function generatePostBody. Detail: ${err}`);
    throw err;
  }
};

const loadDataFromTxt = async (path) => {
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'utf8', function (err, data) {
      if (err) {
        reject(err);
      }
      resolve(data.toString().split('\n'));
    });
  });
};

(async () => {
  try {
    postData = await loadDataFromTxt(filePath);
  } catch (error) {
    console.log(`Error occurred in main. Detail: ${error}`);
  }
})(); // iife

module.exports = {
  setJSONBody: setJSONBody,
  logHeaders: logHeaders,
  generatePostBody: generatePostBody,
};
